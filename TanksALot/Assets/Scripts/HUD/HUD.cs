using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace DefaultNamespace.HUD {
	public class HUD : MonoBehaviour {
		[SerializeField]
		private TextMeshProUGUI basicScore;

		[SerializeField]
		private TextMeshProUGUI health;

		[SerializeField]
		private GameObject basicHolder;

		[Space(10)]
		[SerializeField]
		private TextMeshProUGUI betterScore;

		[SerializeField]
		private Image healthBar;
		
		[SerializeField]
		private GameObject betterHolder;
		
		private TextMeshProUGUI _activeScore;

		private bool _betterHUD;
		
		private Canvas _canvas;

		private CanvasGroup _canvasGroup;
		
//---------------------------------------------------------------------------------------
		private void Awake() {
			_canvas = GetComponent<Canvas>();
			_canvasGroup = GetComponent<CanvasGroup>();

			_canvas.enabled = false;
		}

//---------------------------------------------------------------------------------------
		public void Init() {
			_betterHUD = LoveSettings.Instance.BetterHUD;
			if (_betterHUD) {
				_activeScore = betterScore;
				basicHolder.SetActive(false);
			}
			else {
				_activeScore = basicScore;
				betterHolder.SetActive(false);
			}
		}
		
//---------------------------------------------------------------------------------------
		public void Display(bool shouldDisplay = true) {
			_canvas.enabled = shouldDisplay;
		}
		
//---------------------------------------------------------------------------------------
		public void SetScore(int value) {
			_activeScore.text = value.ToString();
		}
		
//---------------------------------------------------------------------------------------
		public void SetHealth(int value) {
			if (_betterHUD) {
				healthBar.DOFillAmount(value / (float) 10, 0.5f);
//				healthBar.fillAmount = value / (float)10;
			}
			else {
				health.text = value.ToString();
			}
		}
	}
}