using System;
using Baddies;
using Bullets;
using DefaultNamespace.Attract;
using DG.Tweening;
using Explosions;
using UnityEngine;

namespace DefaultNamespace {
    public class GameController : MonoBehaviour {
        [SerializeField]
        private int score;

		[SerializeField]
		private int maxHealth;
		public int MaxHealth => maxHealth;
        
        [Header("Class references")]
        public Player.Player Player;

        public BulletHandler BulletHandler;
        
        public ExplosionHandler ExplosionHandler;

        public BaddieHandler BaddieHandler;

		public HUD.HUD HUD;

		public TitleScreen TitleScreen;

		public GameOverHandler GameOverHandler;

		[Header("Post Effects")]
		public CameraFilterPack_FX_EarthQuake ScreenShake;
		public CameraFilterPack_Blur_Blurry Blur;
		public CameraFilterPack_Color_Chromatic_Aberration ChromaticAberration;
        public static GameController Instance;

//---------------------------------------------------------------------------------------
        private void Awake() {
            if (Instance == null) {
                Instance = this;
            }
            else {
                Destroy(gameObject);
            }
        }

//---------------------------------------------------------------------------------------
        private void Start() {
//This ensures our game autoruns if the title screen is turned off, removes a small layer of
//friction (i.e. pressing the play button)
			if (TitleScreen.gameObject.activeSelf == false) {
//Little delay to give time for everything to set itself up
				DOVirtual.DelayedCall(0.1f, StartGame);
			}

			if (LoveSettings.Instance.UseCoverAll) {
				CoverAll.Instance.FadeDown();
			}
			else {
				CoverAll.Instance.Hide();
			}
			
			HUD.Init();
        }

//---------------------------------------------------------------------------------------
        public void StartGame() {
//Reset all our gameplay vars
            score = 0;
			HUD.SetScore(score);			
			HUD.SetHealth(maxHealth);
			HUD.Display();
			
//Start all our game classes            
            Player.Init();
            BulletHandler.Init();
            ExplosionHandler.Init();
            BaddieHandler.Init();
        }
        
//---------------------------------------------------------------------------------------
		public void IncScore(int value) {
			score += value;
			HUD.SetScore(score);			
		}

//---------------------------------------------------------------------------------------
		public void SetHealth(int health) {
			if (health <= 0) {
				health = 0;
//Game over man!				
				HUD.Display(false);
				GameOverHandler.GameOver(score);
			}
			
			HUD.SetHealth(health);
		}

//---------------------------------------------------------------------------------------
		public void TriggerScreenShake(bool shouldEnable=true) {
			ScreenShake.enabled = shouldEnable;
			Blur.enabled = shouldEnable;
		}
		
//---------------------------------------------------------------------------------------
		public void TriggerScreenShakeWithTimer(float duration) {
			ScreenShake.enabled = true;
			Blur.enabled = true;
			ChromaticAberration.enabled = true;
			
			DOVirtual.DelayedCall(duration, () => {
				ScreenShake.enabled = false;
				Blur.enabled = false;
				ChromaticAberration.enabled = false;
			});
		}

//---------------------------------------------------------------------------------------
		public void TriggerScreenShakeWithTimerNoBlur(float duration) {
			ScreenShake.enabled = true;
			ChromaticAberration.enabled = true;

			DOVirtual.DelayedCall(duration, () => {
				ScreenShake.enabled = false;
				ChromaticAberration.enabled = false;
			});
		}
		
    }
}