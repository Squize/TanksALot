using System;
using DG.Tweening;
using UnityEngine;

namespace DefaultNamespace {
    public class CoverAll : MonoBehaviour{
        private Canvas _canvas;
        private CanvasGroup _canvasGroup;

        public static CoverAll Instance;

//---------------------------------------------------------------------------------------
        private void Awake() {
            if (Instance == null) {
                Instance = this;
                
                _canvas = GetComponent<Canvas>();
                _canvasGroup = GetComponent<CanvasGroup>();
            }
            else {
                Destroy(gameObject);
            }
        }
   
//---------------------------------------------------------------------------------------
        public void FadeDown(float delay=0.5f) {
            _canvasGroup.DOFade(0, 0.4f)
                .SetDelay(delay)
                .OnComplete(() => {
                    _canvas.enabled = false;
            });
        }   

//---------------------------------------------------------------------------------------
        public void FadeUp(Action callBack=null) {
            _canvasGroup.alpha = 0;
            _canvas.enabled = true;

            _canvasGroup.DOFade(1, 0.4f).OnComplete(() => {
                callBack?.Invoke();
            });
        }        
//---------------------------------------------------------------------------------------
        public void Hide() {
            _canvas.enabled = false;
        }
    }
}