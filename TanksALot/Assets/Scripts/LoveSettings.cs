using UnityEngine;

namespace DefaultNamespace {
	public class LoveSettings : MonoBehaviour {
//Just a big dumb list of public bools
		[Space(10)]
		public bool TurretRecoil;

		[Space(10)]
		public bool ShowShadows;
		[SerializeField]
		private GameObject shadowTilemapHolder;

		[Space(10)]
		public bool SparksOnImpact;

		[Space(10)]
		public bool UseCoverAll;

		[Space(10)]
		public bool BetterTopAndTails;

		[Space(10)]
		public bool Retro;
		[SerializeField]
		private CameraFilterPack_TV_ARCADE scanlines;
		[SerializeField]
		private CameraFilterPack_Colors_Brightness brightness;

		[Space(10)]
		public bool ScreenShake;

		[Space(10)]
		public bool BetterBullets;

		[Space(10)]
		public bool BetterExplosions;
		
		[Space(10)]
		public bool BetterDeath;

		[Space(10)]
		public bool BetterHUD;
		
		public static LoveSettings Instance;

//---------------------------------------------------------------------------------------
		private void Awake() {
			if (Instance == null) {
				Instance = this;
				
				shadowTilemapHolder.SetActive(ShowShadows);

				scanlines.enabled = Retro;
				brightness.enabled = Retro;
			}
			else {
				Destroy(gameObject);
			}
		}
		
	}
}