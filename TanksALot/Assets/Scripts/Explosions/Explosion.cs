using System;
using DefaultNamespace;
using UnityEngine;

namespace Explosions {
    public class Explosion : MonoBehaviour{
        [HideInInspector]
        public int ID;

        public bool IsSmallExplosion;

        [SerializeField]
        private ParticleSystem sparksPFX;

        private bool _waitForSparks;

        [SerializeField]
        private ParticleSystem debrisPFX;
        
        [SerializeField]
        private Sprite[] frames;

        [SerializeField]
        private int _animOffset;
        private float _animFlipFlop;
        
        private SpriteRenderer _spriteRenderer;
        
        [HideInInspector]
        public ExplosionHandler Owner;
        
//---------------------------------------------------------------------------------------
        private void Awake() {
            _spriteRenderer = GetComponent<SpriteRenderer>();
        }
        
//---------------------------------------------------------------------------------------
        public void Init(Vector2 pos) {
            transform.position = pos;
            _animOffset = 0;
            _animFlipFlop = 0;

            _spriteRenderer.sprite = frames[0];
            _spriteRenderer.enabled = true;

            gameObject.hideFlags = HideFlags.None;
            gameObject.SetActive(true);

            if (LoveSettings.Instance.SparksOnImpact) {
                if (sparksPFX != null) {
                    sparksPFX.Play();
                    _waitForSparks = true;
                }
            }

            if (IsSmallExplosion == false) {
                if (LoveSettings.Instance.BetterExplosions) {
                    debrisPFX.Play();
                }
            }
        }

//---------------------------------------------------------------------------------------
        public void SetMaterial(Material glowMat) {
            _spriteRenderer.material = glowMat;
        }
        
//---------------------------------------------------------------------------------------
        public void Mainloop(float delta) {
//We don't want to run the animation on every frame            
            _animFlipFlop += delta;
            if (_animFlipFlop < 0.02f) return;
            _animFlipFlop = 0;
            
            if (++_animOffset >= frames.Length) {
//Are we waiting for sparks?
                if (_waitForSparks == false) {
//Finished
                    gameObject.hideFlags = HideFlags.HideInHierarchy;
                    gameObject.SetActive(false);
                    Owner.ReturnToPool(ID,IsSmallExplosion);
                }
                else {
                    _spriteRenderer.enabled = false;
                    if (sparksPFX.isEmitting == false) {
                        gameObject.hideFlags = HideFlags.HideInHierarchy;
                        gameObject.SetActive(false);
                        Owner.ReturnToPool(ID,IsSmallExplosion);
                    }
                }
            }
            else {
                _spriteRenderer.sprite = frames[_animOffset];
            }
        }
    }
}