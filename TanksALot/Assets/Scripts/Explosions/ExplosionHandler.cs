using System;
using DefaultNamespace;
using UnityEngine;

namespace Explosions {
    public class ExplosionHandler : MonoBehaviour{
        [SerializeField]
        private Explosion smallExplosionRef;

        [SerializeField]
        private Explosion largeExplosionRef;

        [SerializeField]
        private Material glowingSpriteMaterial;

        [SerializeField]
        private ParticleSystem scorchMarkPFX;

        private const int MAXSMALLEXPLOSIONS = 50;

        private const int MAXSLARGEEXPLOSIONS = 5;

        private Explosion[] _smallExplosionPool;
        private int[] _smallExplosionPoolOffset;

        private Explosion[] _largeExplosionPool;
        private int[] _largeExplosionPoolOffset;
        
//---------------------------------------------------------------------------------------
        private void Awake() {
//We're going to start by populating our pools
            _smallExplosionPool = new Explosion[MAXSMALLEXPLOSIONS];
            _smallExplosionPoolOffset = new int[MAXSMALLEXPLOSIONS];

            Explosion clone;

            int cnt = -1;
            while (++cnt!=MAXSMALLEXPLOSIONS) {
                clone = Instantiate(smallExplosionRef, transform);
                clone.ID = cnt;
                clone.Owner = this;
                
                _smallExplosionPool[cnt] = clone;
                _smallExplosionPoolOffset[cnt] = cnt;
                
                clone.gameObject.SetActive(false);
//We don't want to see a million cloned explosions in the Hierarchy, it's super messy
                clone.gameObject.hideFlags = HideFlags.HideInHierarchy;
            }
            
            Destroy(smallExplosionRef.gameObject);

//Do the same for the large explosions
            _largeExplosionPool = new Explosion[MAXSLARGEEXPLOSIONS];
            _largeExplosionPoolOffset = new int[MAXSLARGEEXPLOSIONS];
            
            cnt = -1;
            while (++cnt!=MAXSLARGEEXPLOSIONS) {
                clone = Instantiate(largeExplosionRef, transform);
                clone.ID = cnt;
                clone.Owner = this;
                
                _largeExplosionPool[cnt] = clone;
                _largeExplosionPoolOffset[cnt] = cnt;
                
                clone.gameObject.SetActive(false);
                clone.gameObject.hideFlags = HideFlags.HideInHierarchy;
            }
            
            Destroy(largeExplosionRef.gameObject);

            enabled = false;
        }

//---------------------------------------------------------------------------------------
        public void Init() {
            if (LoveSettings.Instance.BetterExplosions) {
//Update the material in all our explosions
                foreach (var i in _smallExplosionPool) {
                    i.SetMaterial(glowingSpriteMaterial);
                }
                
                foreach (var i in _largeExplosionPool) {
                    i.SetMaterial(glowingSpriteMaterial);
                }
            }
            
            scorchMarkPFX.Clear();
            
            enabled = true;
        }

//---------------------------------------------------------------------------------------
        private void Update() {
            float delta = Time.deltaTime;
            
            int cnt = -1;
            while (++cnt!=MAXSMALLEXPLOSIONS) {
                if (_smallExplosionPoolOffset[cnt] == -1) {
                    _smallExplosionPool[cnt].Mainloop(delta);
                }
            }
            
            cnt = -1;
            while (++cnt!=MAXSLARGEEXPLOSIONS) {
                if (_largeExplosionPoolOffset[cnt] == -1) {
                    _largeExplosionPool[cnt].Mainloop(delta);
                }
            }
            
        }

//---------------------------------------------------------------------------------------
        public void RequestSmallExplosion(Vector2 pos) {
            Explosion explosion = getSmallExplosionFromPool();
            if (explosion != null) {
                explosion.Init(pos);
            }
        }

//---------------------------------------------------------------------------------------
        public void RequestTankHitExplosion(Vector2 pos) {
            Explosion explosion = getSmallExplosionFromPool();
            if (explosion != null) {
                explosion.Init(pos);
                
                if (LoveSettings.Instance.ScreenShake) {
                    GameController.Instance.TriggerScreenShakeWithTimerNoBlur(0.2f);
                }
            }
        }

//---------------------------------------------------------------------------------------
        public void RequestTankDestroyedExplosion(Vector2 pos) {
            Explosion explosion = getLargeExplosionFromPool();
            if (explosion != null) {
                explosion.Init(pos);

                if (LoveSettings.Instance.ScreenShake) {
                    GameController.Instance.TriggerScreenShakeWithTimer(0.3f);
                }
                
                if (LoveSettings.Instance.BetterExplosions) {
//Plot our scorch mark, just using a particle effect
                    var emitParams = new ParticleSystem.EmitParams();
                    emitParams.position = pos;
                    scorchMarkPFX.Emit(emitParams,1);
                }
                
            }
        }
        
//---------------------------------------------------------------------------------------
        public void ReturnToPool(int id,bool isSmallExplosion) {
            if (isSmallExplosion) {
                _smallExplosionPoolOffset[id] = id;
            }
            else {
                _largeExplosionPoolOffset[id] = id;
            }
        }
        
//---------------------------------------------------------------------------------------
        private Explosion getSmallExplosionFromPool() {
            int cnt = -1;
            while (++cnt!=MAXSMALLEXPLOSIONS) {
                if (_smallExplosionPoolOffset[cnt] != -1) {
//This bullet is available, so lets use it
                    _smallExplosionPoolOffset[cnt] = -1;
                    return _smallExplosionPool[cnt];
                }
            }
            
            return null;
        }

//---------------------------------------------------------------------------------------
        private Explosion getLargeExplosionFromPool() {
            int cnt = -1;
            while (++cnt!=MAXSLARGEEXPLOSIONS) {
                if (_largeExplosionPoolOffset[cnt] != -1) {
//This bullet is available, so lets use it
                    _largeExplosionPoolOffset[cnt] = -1;
                    return _largeExplosionPool[cnt];
                }
            }
            
            return null;
        }
        
    }
}