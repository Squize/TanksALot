using System;
using System.Collections.Generic;
using TMPro;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;

namespace Baddies {
	public class Steering : MonoBehaviour {
		public bool DebugFlag;

		[Space(10)] 
		[SerializeField] private int resolution = 8;

		[Space(10)]
		[SerializeField]
		private LayerMask avoidanceMask;

		[SerializeField]
		private TextMeshPro[] textStorage;
		
//These are our maps
		[SerializeField]
		private float[] _interestVectors;
		
		[SerializeField]
		private float[] _dangerVectors;

		private List<int> _zeroValueStorage;
		
		private float _distanceToCheck = 3f;

		private const float Radius = 0.5f;
		
		private Vector2[] _rayDirections;
		private float _viewAngle;
		
		private int _travellingSouthCnt;
		
//---------------------------------------------------------------------------------------
		void Awake() {
			_interestVectors = new float[resolution];
			_dangerVectors = new float[resolution];

			_rayDirections = new Vector2[resolution];

			_zeroValueStorage = new List<int>(resolution);
			
			_viewAngle = 360 / resolution;
			
			setRayDirections();
		}

//---------------------------------------------------------------------------------------
		private void setRayDirections() {
			int cnt = -1;
			while (++cnt!=resolution) {
//Calculate the direction of the line around the circle
				float viewAngleUpdated = _viewAngle * cnt;
//Calculate the angle of the line around the circle
				Vector2 viewAngleDir = DirFromAngle(viewAngleUpdated);
				_rayDirections[cnt] = viewAngleDir * _distanceToCheck;

				textStorage[cnt].text = cnt.ToString();
				textStorage[cnt].transform.position = _rayDirections[cnt];
			}
		}

//---------------------------------------------------------------------------------------
		private Vector2 DirFromAngle(float angleInDegrees) {
			return new Vector2(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
		}

//---------------------------------------------------------------------------------------
		public Vector2 GetDirectionVectors(Vector2 targetPos, Vector2 agentPos, float rayDistance = 5f) {
			_distanceToCheck = rayDistance;

			RaycastHit2D hit;
			
			int cnt = -1;
			while (++cnt != resolution) {
//@see https://manski.net/2012/12/net-array-clear-vs-arrayx-0-performance/ 
				_dangerVectors[cnt] = 0;
				_interestVectors[cnt] = 0;		//Clear these here as we're in a loop anyway
			
				hit = Physics2D.CircleCast(agentPos, Radius,_rayDirections[cnt], _distanceToCheck, avoidanceMask);
				if (hit.collider!=null) {
					if (hit.transform.gameObject.layer == 6) {
//Normalise the distance
						float normalised = hit.distance / _distanceToCheck;
//We want the danger values to go from 0 (No danger) to 1 (Very dangerous)
						_dangerVectors[cnt] = 1-normalised;
					}
				}
			}

//--------------
// Interest maps
//--------------

//We've now filled our danger map, next lets fill our interest map
			float lowestInterestValue = -5;
			int interestMapOffset = 0;
			float currentInterestVector;
			float dangerValue;
			
			_zeroValueStorage.Clear();
			
			Vector2 dirToTarget = targetPos - agentPos;
			dirToTarget = dirToTarget.normalized;
			
			cnt = -1;
			while (++cnt != resolution) {
				float distance = Vector2.Distance(dirToTarget,_rayDirections[cnt].normalized );
				currentInterestVector = 1-distance;
				
				dangerValue = _dangerVectors[cnt];
				
				if (dangerValue > 0) {
					currentInterestVector -= dangerValue;
				}
				else {
					_zeroValueStorage.Add(cnt);
				}
				
				_interestVectors[cnt] = currentInterestVector;
				
				if (currentInterestVector > lowestInterestValue) {
					lowestInterestValue = currentInterestVector;
					interestMapOffset = cnt;
				}
			}
		
//-----------------
// Best alternative
//-----------------
			
//ok if we're at this point there's no really good direction to take, so we check to see which of the zero values is better
			int len = _zeroValueStorage.Count;

			if (len == 1) {
//Early out
				interestMapOffset = _zeroValueStorage[0];
				
				if (Debug.isDebugBuild) {
					if (DebugFlag) {
						Debug.DrawRay(agentPos,_rayDirections[interestMapOffset]*0.5f,Color.cyan);
					}
				}

				return _rayDirections[interestMapOffset].normalized;
			}

//If we're here we're only left with "low quality" directions
			float dangerTotal = 0;
			float lowestDanager = 2;
			int offset;
	
//So we want to see which one has the least danger either side of it, i.e. the safest option			
			cnt = -1;
			while (++cnt!=len) {
				dangerTotal = 0;
				offset = _zeroValueStorage[cnt];
				if (offset > 0) {
					dangerTotal += _dangerVectors[offset - 1];
				}

				if (offset < resolution-1) {
					dangerTotal += _dangerVectors[offset + 1];
				}

				if (dangerTotal < lowestDanager) {
					lowestDanager = dangerTotal;
					interestMapOffset = offset;
				}
			}
			
			if (Debug.isDebugBuild) {
				if (DebugFlag) {
					Debug.DrawRay(agentPos,_rayDirections[interestMapOffset]*0.5f,Color.magenta);
				}
			}

			return _rayDirections[interestMapOffset].normalized;
		}
	}
}
