using System;
using DefaultNamespace;
using DG.Tweening;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

namespace Baddies {
    public class Baddie : MonoBehaviour {
        public enum State {
            Nop,
            LookingForPlayer,
            Active,
            ArrivedAtTarget,
            Dying,
            BetterDeath,
            Dead
        }

        public State CurrentState;

        [Header("Visual Elements")] 
        [SerializeField]
        private Transform turretHolder;

		[SerializeField] 
		private GameObject shadow;

		[SerializeField] 
		private GameObject tank;

        [SerializeField] 
        private Transform gunBarrel;
        private Vector2 _gunBarrelDefaultPos;

        [SerializeField] 
        private SpriteRenderer turretRenderer;
        private SpriteRenderer _tankRenderer;
        private SpriteRenderer _barrelRenderer;

        [Header("VFX")] 
        [SerializeField] 
        private ParticleSystem barrelSmokePFX;
        
//Targeting properties		
        private const float DistanceToStartTracking = 11;
        private const float DistanceToRespawn = 13;

        private Vector2 _targetPos;

        private float _steeringCnt;

        private float _waitingCnt;
        private float _waitingDelay = 2;
        
        [Header("Shooting Properties")] 
        [SerializeField] 
        private LayerMask aimingLayerMask;

        [SerializeField] 
        private Transform shootPoint;
        
        [HideInInspector]
        public float ReloadTime;
        private float _currentReloadTime;
		
//Default position properties
        private Vector2 _startPos;
        private Vector3 _startRot;
        private Vector3 _turrentStartRot;
        
//Health properties
        [HideInInspector]
        public int MaxHealth;
        
//Display this in the inspector for debugging 
        [SerializeField]
        private int _health;

        private float _waitingToDieCnt;
        
//Components		
        private Rigidbody2D _rigidbody;
        private NavMeshAgent _agent;

		private BoxCollider2D _collider;
		
//External refs        
        [HideInInspector]
        public BaddieHandler Owner;
        [HideInInspector]
        public Transform Player;

        private LoveSettings _loveSettings;

//---------------------------------------------------------------------------------------
        private void Awake() {
            _rigidbody = GetComponent<Rigidbody2D>();
            _collider = GetComponent<BoxCollider2D>();
            
            _agent = GetComponent<NavMeshAgent>();
            _agent.updateRotation = false;
            _agent.updateUpAxis = false;
			
            _startPos = transform.position;
            _startRot = transform.localEulerAngles;
            _turrentStartRot = turretHolder.localEulerAngles;
            
            _gunBarrelDefaultPos = gunBarrel.localPosition;

            _tankRenderer = tank.GetComponent<SpriteRenderer>();
            _barrelRenderer=gunBarrel.GetComponent<SpriteRenderer>();
        }

//---------------------------------------------------------------------------------------
        public void Init() {
            _loveSettings = LoveSettings.Instance;

//Reset our position / rotation
            transform.position = _startPos;
            transform.localEulerAngles = _startRot;
            turretHolder.localEulerAngles = _turrentStartRot;

            _rigidbody.velocity = Vector2.zero;
            _rigidbody.angularVelocity = 0;
            _rigidbody.rotation = 0;
            _rigidbody.MovePosition(_startPos);
 
            _collider.enabled = true;
            _rigidbody.simulated = true;

//Reset the colours in case we've darkened them            
            _tankRenderer.color=Color.white;
            _barrelRenderer.color=Color.white;
            turretRenderer.color=Color.white;
            
//Reset all our vars            
            _health = MaxHealth;

            _currentReloadTime = ReloadTime;

            _steeringCnt = 0;
            _waitingCnt = 0;

            _waitingToDieCnt = 0;
            
//Turn everything back on            
            _agent.enabled = true;
            _agent.ResetPath();
            
            turretHolder.gameObject.SetActive(true);
            shadow.gameObject.SetActive(true);
            tank.gameObject.SetActive(true);
            
            CurrentState = State.LookingForPlayer;
        }        

//---------------------------------------------------------------------------------------
        public void Hit(int bulletPower,Vector2 impactPoint) {
            _health -= bulletPower;

            if (_health <= 0) {
//We're dead!
                Owner.BaddieDestroyed(transform.position);
                CurrentState = State.Dying;
            }
            else {
                Owner.BaddieHit(impactPoint);
            }
            
//It's possible we can get a sneaky shot in before this baddie is tracking the player
//if so then wake him up and start attacking the player straight away
            if (CurrentState == State.LookingForPlayer) {
                setTargetPosition();
                CurrentState = State.Active;
            }
        }
        
//---------------------------------------------------------------------------------------
        public void MainLoop() {
            if (CurrentState == State.Nop) return;

            switch (CurrentState) {
                case State.LookingForPlayer:
                    lookForPlayer();
                    break;
                case State.Active:
                    trackPlayer();
                    rotateTurretTowardsPlayer();
                    break;
                case State.ArrivedAtTarget:
                    arrivedAtTarget();
                    rotateTurretTowardsPlayer();
                    break;
                case State.Dying:
                    killed();
                    break;
               case State.BetterDeath:
                    waitingToDie();
                    break;
				case State.Dead:
					dead();
					break;
            }
        }
        
//---------------------------------------------------------------------------------------
        private void lookForPlayer() {
//We just want to do a distance check from this baddie to the player, we don't care
//about a line of sight
            float distance = Vector2.Distance(transform.position, Player.position);

            if (distance <= DistanceToStartTracking) {
//We're close enough to care about the player, so change the state
                CurrentState = State.Active;
//...and pick a position to move to
                setTargetPosition();
            }
        }
        
//---------------------------------------------------------------------------------------
        private void setTargetPosition() {
            _targetPos = Player.position;
            _targetPos += Random.insideUnitCircle * 3;
            
            _agent.SetDestination(_targetPos);
        }
        
//---------------------------------------------------------------------------------------
        private void trackPlayer() {
//Rotate in the direction of travel
            Vector3 velocity = _agent.velocity.normalized;

	        if (_agent.velocity.sqrMagnitude > Mathf.Epsilon) {
                Vector3 rotDir = new Vector3(0, 0, velocity.y);
                if (rotDir != Vector3.zero) {
                    _rigidbody.SetRotation(Quaternion.LookRotation(rotDir));
                }
            }

//Get the distance to the target, if it's close enough, pick a new target
            float distance = Vector2.Distance(_targetPos, transform.position);

            if (distance < 2f) {
                _waitingCnt = 0;
                _steeringCnt = 0;
                CurrentState = State.ArrivedAtTarget;
            }
            else {
//It's possible that we can never get close enough to the target, so fall back to having
//a timer to pick a new timer
                _steeringCnt += Time.deltaTime;
                if (_steeringCnt > 5f) {
                    _steeringCnt = 0;
                    setTargetPosition();
                }
            }
        }

//---------------------------------------------------------------------------------------
        private void arrivedAtTarget() {
            _waitingCnt += Time.deltaTime;
            if (_waitingCnt >= _waitingDelay) {
                _waitingCnt = 0;
                setTargetPosition();
                
                CurrentState = State.Active;
            }
        }

//---------------------------------------------------------------------------------------
        private void rotateTurretTowardsPlayer() {
            Vector2 diff = Player.position - turretHolder.position;
            diff.Normalize ();
            float rot = Mathf.Atan2 (diff.y, diff.x) * Mathf.Rad2Deg;

            turretHolder.rotation = Quaternion.Euler (0f, 0f, rot); 
            
//Test for shooting
            _currentReloadTime -= Time.deltaTime;
            if (_currentReloadTime < 0) {
                _currentReloadTime = ReloadTime;
                testForShooting();
            }
        }
        
//---------------------------------------------------------------------------------------
        private void killed() {
//Kill any physics we may have running            
            _rigidbody.velocity = Vector2.zero;
            _rigidbody.angularVelocity = 0;
            _rigidbody.rotation = 0;

			_collider.enabled = false;
            _rigidbody.simulated = false;

//Disable the nav agent			
            _agent.isStopped = true;
            _agent.enabled = false;

//Even if we're running the better death we want to turn the shadow off during the explosion            
            shadow.gameObject.SetActive(false);

            if (LoveSettings.Instance.BetterDeath) {
                _tankRenderer.color=Color.black;
                _barrelRenderer.color=Color.black;
                turretRenderer.color=Color.black;
                CurrentState = State.BetterDeath;
            }
            else {
//Turn off all the visual elements
                turretHolder.gameObject.SetActive(false);
                tank.gameObject.SetActive(false);

                CurrentState = State.Dead;
            }
        }

//---------------------------------------------------------------------------------------
        private void waitingToDie() {
            _waitingToDieCnt += Time.deltaTime;
            if(_waitingToDieCnt<=0.25f) return;

            _waitingToDieCnt = 0;
//Now we can turn off all the visual elements
            turretHolder.gameObject.SetActive(false);
            tank.gameObject.SetActive(false);

            CurrentState = State.Dead;
        }
        
//---------------------------------------------------------------------------------------
//The dead state is waiting for a chance to re-spawn
		private void dead() {
            float distance = Vector2.Distance(_startPos, Player.position);

            if (distance >= DistanceToRespawn) {
//We should be off screen now, so we can reset everything
                Init();
            }
			
		}
		
//---------------------------------------------------------------------------------------
        private void testForShooting() {
            Vector2 dir = Player.position - turretHolder.position;

//Firstly, lets cast a ray to see if we can avoid hitting the wall
            RaycastHit2D hit = Physics2D.Linecast(turretHolder.position, Player.position, aimingLayerMask);
            if (hit.collider != null) {
//Only shoot if we've got a clear shot to the player                
                if (hit.collider.gameObject.layer == 3) {
                    GameController.Instance.BulletHandler.RequestBullet(shootPoint.position,dir,false);
                    
//Add recoil
                    if (_loveSettings.TurretRecoil) {
                        barrelSmokePFX.Play();
                        
                        gunBarrel.DOLocalMoveX(0.23f, 0.05f)
                            .SetEase(Ease.OutExpo)
                            .OnComplete(() => {
//Restore it
                                gunBarrel.DOLocalMove(_gunBarrelDefaultPos, 0.15f)
                                    .SetEase(Ease.InExpo);
                            });
                    }
                    
                }
            }
        }
    }
}