using System;
using DefaultNamespace;
using DefaultNamespace.HUD;
using Explosions;
using UnityEngine;

namespace Baddies {
    public class BaddieHandler : MonoBehaviour {
        public enum State {
            Nop,
            Active,
        }

        public State CurrentState;

        [SerializeField] 
        private Baddie[] baddies;

        [Header("Shooting Properties")]
        [Range(0.1f,2)]
        [SerializeField] 
        private float reloadTime;
        private float _currentReloadTime;

        [Header("Health Properties")]
        [SerializeField]
        private int health;
        
//External classes        
        private Player.Player _player;

        private ExplosionHandler _explosionHandler;
        
        private GameController _gameController;
        
//---------------------------------------------------------------------------------------
        private void Start() {
            _gameController=GameController.Instance;
            
            _player = _gameController.Player;
            _explosionHandler = _gameController.ExplosionHandler;
            
//Populate the baddies with the data they need
            foreach (var i in baddies) {
//Setting each value like this is a little gross, ideally it should all be in a
//scriptable object, but got to share this in less than two days, so no time.
//(Working rough code is always better than pretty non-working code)
                i.Owner = this;
                i.Player = _player.transform;
                i.ReloadTime = reloadTime;
                i.MaxHealth = health;
            }
        }

//---------------------------------------------------------------------------------------
        public void Init() {
            foreach (var i in baddies) {
                i.Init();
            }

            CurrentState = State.Active;
        }

//---------------------------------------------------------------------------------------
        private void Update() {
            if (CurrentState == State.Nop) return;

            if (CurrentState == State.Active) {
//Loop through all the baddies calling their mainloop
                foreach (var i in baddies) {
                    i.MainLoop();
                }
            }
        }
        
//---------------------------------------------------------------------------------------
        public void BaddieHit(Vector2 baddiePos) {
            _explosionHandler.RequestTankHitExplosion(baddiePos);
            _gameController.IncScore(100);
        }
        
//---------------------------------------------------------------------------------------
        public void BaddieDestroyed(Vector2 baddiePos) {
            _explosionHandler.RequestTankDestroyedExplosion(baddiePos);
            _gameController.IncScore(500);
        }
        
    }
}
