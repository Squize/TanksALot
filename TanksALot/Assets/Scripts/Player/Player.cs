using System;
using Bullets;
using DefaultNamespace;
using DG.Tweening;
using Unity.VisualScripting;
using UnityEngine;

namespace Player {
    public class Player : MonoBehaviour {
        public enum State {
            Nop,
            Active,
            Dead
        }

        public State CurrentState;

        [Space(10)]
        [SerializeField]
        private Camera camera;

		private int _health;
		
        [Header("Visual Elements")] 
        [SerializeField]
        private Transform turretHolder;

		[SerializeField] 
		private GameObject shadow;

		[SerializeField] 
		private GameObject tank;

		[SerializeField] 
		private Transform gunBarrel;

		private Vector2 _gunBarrelDefaultPos;

		[Header("VFX")] 
		[SerializeField] 
		private ParticleSystem barrelSmokePFX;
		
        [Header("Shooting Properties")] 
        [SerializeField] 
        private Transform shootPoint;

        private float _shootDirectionAngle;
        
        [SerializeField] 
        [Range(0.1f,2)]
        private float reloadTime;

        private float _currentReloadTime;
        
//Movement properties
        [Header("Movement Properties")] 
        [SerializeField]
        private float speed;
        [SerializeField]
        private float reverseSpeed;
        
        [SerializeField]
        private float turnSpeed;

        private float _movementInputValue;
        private float _turnInputValue;

//Default pos / rot values so we can reset the game correctly        
        private Vector2 _defaultPos;
        private Vector2 _defaultRot;
        private Vector2 _defaultTurretRot;

//Components		
		private Rigidbody2D _rigidbody;
		private BoxCollider2D _collider;

//External class refs        
        private BulletHandler _bulletHandler;

		private GameController _gameController;

		private LoveSettings _loveSettings;
		
//---------------------------------------------------------------------------------------
        private void Awake() {
            _rigidbody = GetComponent<Rigidbody2D>();
			_collider = GetComponent<BoxCollider2D>();

//Store the default pos / rot values            
            _defaultPos = _rigidbody.position;
            _defaultRot = transform.localEulerAngles;
            _defaultTurretRot = turretHolder.localEulerAngles;

			_gunBarrelDefaultPos = gunBarrel.localPosition;
		}

//---------------------------------------------------------------------------------------
        private void Start() {
//Get our references here, it gives Unity time to have called all the Awake methods
			_loveSettings = LoveSettings.Instance;
			_gameController = GameController.Instance;
            _bulletHandler = _gameController.BulletHandler;
        }

//---------------------------------------------------------------------------------------
        public void Init() {
//Reset all our positions / rotations
            _rigidbody.position = _defaultPos;
			transform.position = _defaultPos;
            transform.localEulerAngles = _defaultRot;
            turretHolder.localEulerAngles = _defaultTurretRot;

			gunBarrel.localPosition = _gunBarrelDefaultPos;
			
			_health = GameController.Instance.MaxHealth;
			
			_collider.enabled = true;
			_rigidbody.simulated = true;
            
			turretHolder.gameObject.SetActive(true);
			shadow.gameObject.SetActive(true);
			tank.gameObject.SetActive(true);
			
            CurrentState = State.Active;
        }

//---------------------------------------------------------------------------------------
		public void Hit(int bulletPower,Vector2 impactPoint) {
			_health -= bulletPower;
			if (_health <= 0) {
				_health = 0;
//We're dead! Bummer				
//Kill any physics we may have running            
				_rigidbody.velocity = Vector2.zero;
				_rigidbody.angularVelocity = 0;
				_rigidbody.rotation = 0;

				_collider.enabled = false;
				_rigidbody.simulated = false;
            
//Turn off all the visual elements
				turretHolder.gameObject.SetActive(false);
				shadow.gameObject.SetActive(false);
				tank.gameObject.SetActive(false);

				CurrentState = State.Dead;
				
				_gameController.ExplosionHandler.RequestTankDestroyedExplosion(transform.position);	
			}
			else {
				_gameController.ExplosionHandler.RequestTankHitExplosion(impactPoint);
			}
			
			GameController.Instance.SetHealth(_health);

		}
		
//---------------------------------------------------------------------------------------
        private void Update() {
            if (CurrentState == State.Nop) return;

            if (CurrentState == State.Active) {
                handleInput();
                rotateTowardsMouse();
                _currentReloadTime -= Time.deltaTime;
                if (_currentReloadTime < 0) {
                    testForShooting();
                }
            }
        }

//---------------------------------------------------------------------------------------
        private void FixedUpdate() {
            if (CurrentState == State.Active) {
                movePlayer();
                turnPlayer();
            }
        }

//---------------------------------------------------------------------------------------
        private void handleInput() {
//Horizontal / Vertical are set up by Unity, mapped to WASD / Arrow keys        
            _movementInputValue = Input.GetAxis("Vertical");
            _turnInputValue = Input.GetAxis("Horizontal");
        }

//---------------------------------------------------------------------------------------
        private void rotateTowardsMouse() {
            Vector3 mousePos = Input.mousePosition;
            mousePos.z = 5.23f;
 
            Vector3 objectPos = camera.WorldToScreenPoint (transform.position);
            mousePos.x -= objectPos.x;
            mousePos.y -= objectPos.y;
 
            _shootDirectionAngle = Mathf.Atan2(mousePos.y, mousePos.x) * Mathf.Rad2Deg;
            turretHolder.rotation = Quaternion.Euler(new Vector3(0, 0, _shootDirectionAngle));
        }
        
//---------------------------------------------------------------------------------------
        private void testForShooting() {
            bool isMouseDown = Input.GetMouseButtonDown(0);
            
//Jump is the default rather than Fire          
            if (Input.GetButtonDown("Jump") || isMouseDown) {
//Reset our reload time so we're not firing a constant stream of bullets
                _currentReloadTime = reloadTime;
                _bulletHandler.RequestBullet(shootPoint.position, shootPoint.right, true);
				
//Add recoil
				if (_loveSettings.TurretRecoil) {
					barrelSmokePFX.Play();
					
					gunBarrel.DOLocalMoveX(0.23f, 0.05f)
						.SetEase(Ease.OutExpo)
						.OnComplete(() => {
//Restore it
							gunBarrel.DOLocalMove(_gunBarrelDefaultPos, 0.15f)
								.SetEase(Ease.InExpo);

						});
				}
			}
        }
        
//---------------------------------------------------------------------------------------
        private void movePlayer() {
//Create a vector in the direction the tank is facing with a magnitude based on the input,
//speed and the time between frames.
            float currentSpeed = speed;
            if (_movementInputValue < 0) {
                currentSpeed = reverseSpeed;
            }

            Vector2 movement = transform.right * (_movementInputValue * currentSpeed) * Time.deltaTime;
            _rigidbody.MovePosition(_rigidbody.position + movement);
        }

//---------------------------------------------------------------------------------------
        private void turnPlayer() {
//The _turnInputValue is set to minus so right is clockwise, feels more natural
            float turn = -_turnInputValue * turnSpeed * Time.deltaTime;
            _rigidbody.MoveRotation(_rigidbody.rotation +turn);
        }

    }
}