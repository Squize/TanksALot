using System;
using UnityEngine;

namespace Bullets {
    public class BulletHandler : MonoBehaviour {
        [SerializeField]
        private Bullet masterBulletRef;

        [Header("Bullet Sprites")]
        public Sprite DefaultBullet;
        public Sprite PlayerBullet;
        public Sprite BaddieBullet;

        [Space(10)]
        public Material DefaultBulletMaterial;
        public Material GlowingBulletMaterial;
        
        [Header("Layer Masks")]
        [SerializeField]
        private LayerMask terrainCollisionsLayerMask;
        [SerializeField]
        private LayerMask playerBulletsMask;
        [SerializeField]
        private LayerMask baddieBulletsMask;
        [SerializeField]
        private LayerMask baddiesMask;
        [SerializeField]
        private LayerMask playerMask;

        private int _terrainCollisionsLayerMask;
        private int _playerBulletsMask;
        private int _baddieBulletsMask;
        private int _baddiesMask;
        private int _playerMask;
        
        private const int MAXBULLETS = 80;
        
        private Bullet[] _pool;
        private int[] _poolOffset;

//---------------------------------------------------------------------------------------
        private void Awake() {
//First, lets get int values for our layer masks so we can pass those to the bullets
//This is for our physics based collision detection
            _terrainCollisionsLayerMask = toLayer(terrainCollisionsLayerMask);
            _playerBulletsMask = toLayer(playerBulletsMask);
            _baddieBulletsMask = toLayer(baddieBulletsMask);
            _baddiesMask = toLayer(baddiesMask);
            _playerMask = toLayer(playerMask);
            
//We're going to start by populating our pool
            _pool = new Bullet[MAXBULLETS];
            _poolOffset = new int[MAXBULLETS];

            Bullet clone;
            
            int cnt = -1;
            while (++cnt!=MAXBULLETS) {
                clone = Instantiate(masterBulletRef, transform);
                clone.ID = cnt;
                clone.Owner = this;
//Passing over lots of arguments like this is error prone, so look to use something like a struct to it
//all in one hit
                clone.SetLayerMasks(_terrainCollisionsLayerMask,_playerBulletsMask,_baddieBulletsMask,
                    _baddiesMask,_playerMask);
                _pool[cnt] = clone;
                _poolOffset[cnt] = cnt;
                
                clone.gameObject.SetActive(false);
//We don't want to see a million cloned bullets in the Hierarchy, it's super messy
                clone.gameObject.hideFlags = HideFlags.HideInHierarchy;
            }
            
            Destroy(masterBulletRef.gameObject);
            
            
        }

//---------------------------------------------------------------------------------------
        public void Init() {
            
        }
        
//---------------------------------------------------------------------------------------
        public void RequestBullet(Vector2 shotPos, Vector2 direction, bool isPlayer) {
            Bullet bull = getFromPool();
            if (bull != null) {
                bull.Init(shotPos,direction,isPlayer);
            }
        }
        
//---------------------------------------------------------------------------------------
        public void ReturnToPool(int id) {
            _poolOffset[id] = id;
        }
        
//---------------------------------------------------------------------------------------
        private Bullet getFromPool() {
            int cnt = -1;
            while (++cnt!=MAXBULLETS) {
                if (_poolOffset[cnt] != -1) {
//This bullet is available, so lets use it
                    _poolOffset[cnt] = -1;
                    return _pool[cnt];
                }
            }
            
            return null;
        }
        
//---------------------------------------------------------------------------------------
//Converts a LayerMask to an int we can actually use, sigh Unity, it shouldn't be this difficult 
        private int toLayer ( int bitmask ) {
            int result = bitmask>0 ? 0 : 31;
            while( bitmask>1 ) {
                bitmask = bitmask>>1;
                result++;
            }
            return result;
        }		
        
    }
}