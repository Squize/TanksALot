using System;
using System.Collections;
using Baddies;
using DefaultNamespace;
using DG.Tweening;
using UnityEngine;

namespace Bullets {
	public class Bullet : MonoBehaviour {
		[HideInInspector]
		public int ID;

		[SerializeField]
		private ParticleSystem bulletFlamesPFX;
		[SerializeField]
		private ParticleSystem bulletSmokePFX;

		private bool _runningSmokePFX;
		
//Collision masks, set via BulletHandler		
		private int _terrainCollisionsLayerMask;
		private int _playerBulletsMask;
		private int _baddieBulletsMask;
		private int _baddiesMask;
		private int _playerMask;

//Simple flag so we know if this is a bullet the player has fired or not        
		private bool _isPlayer;

		private int _glitchCnt;

		private Vector2 _dir;

//Components        
		private Rigidbody2D _rigidbody;
		private SpriteRenderer _renderer;

		[HideInInspector]
		public BulletHandler Owner;

		private LoveSettings _loveSettings;
		
//---------------------------------------------------------------------------------------
		private void Awake() {
			_rigidbody = GetComponent<Rigidbody2D>();
			_renderer = GetComponent<SpriteRenderer>();
		}

//---------------------------------------------------------------------------------------
//Only called when we create this bullet
		public void SetLayerMasks(int terrainCollisionsLayerMaskRef, int playerBulletsMaskRef, int baddieBulletsMaskRef,
			int baddiesMaskRef, int playerMaskRef) {

			_terrainCollisionsLayerMask = terrainCollisionsLayerMaskRef;
			_playerBulletsMask = playerBulletsMaskRef;
			_baddieBulletsMask = baddieBulletsMaskRef;
			_baddiesMask = baddiesMaskRef;
			_playerMask = playerMaskRef;
			
			_loveSettings=LoveSettings.Instance;
		}

//---------------------------------------------------------------------------------------
		public void Init(Vector2 pos, Vector2 direction, bool isPlayer) {
			_isPlayer = isPlayer;
			_dir = direction.normalized;

//Show it in the Hierarchy when it's active, makes debugging easier            
			gameObject.hideFlags = HideFlags.None;

			_renderer.enabled = true;
			gameObject.SetActive(true);

			float bulletSpeedMultiplier = 0.2f;
//Set the collision layer			

			if (_isPlayer) {
				gameObject.layer = _playerBulletsMask;
				bulletSpeedMultiplier = 0.3f;			//Player bullets move faster

				if (_loveSettings.BetterBullets) {
					_renderer.sprite = Owner.PlayerBullet;
					_renderer.material = Owner.DefaultBulletMaterial;
					bulletSmokePFX.Play();
					bulletFlamesPFX.Play();
					
					_runningSmokePFX = true;
				}
			}
			else {
				gameObject.layer = _baddieBulletsMask;
				_runningSmokePFX = false;
				
				if (_loveSettings.BetterBullets) {
					_renderer.sprite = Owner.BaddieBullet;
					_renderer.material = Owner.GlowingBulletMaterial;
				}
			}

			float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
			_rigidbody.MoveRotation(angle);
			_rigidbody.MovePosition(pos);

			transform.position = pos;
			transform.localEulerAngles = new Vector3(0, 0, angle);

			_rigidbody.simulated = true;

			_rigidbody.AddForce(_dir * bulletSpeedMultiplier, ForceMode2D.Impulse);
		}

//---------------------------------------------------------------------------------------
		private void OnCollisionEnter2D(Collision2D col) {
//We've hit something!

//Lets see what we've hit, we can do it by layer
			int layer = col.gameObject.layer;

			if (layer == _terrainCollisionsLayerMask) {
				GameController.Instance.ExplosionHandler.RequestSmallExplosion(transform.position);
			}
			else {
				if (_isPlayer) {
//Is it player bullet > baddie?
					if (layer == _baddiesMask) {
//Cool we've hit the baddie, let's reduce it's health
						Baddie baddie = col.gameObject.GetComponent<Baddie>();
						baddie.Hit(1, transform.position);
					}

				}
				else {
					if (layer == _playerMask) {
						GameController.Instance.Player.Hit(1, transform.position);
					}
				}
			}

			dispose();
		}

//---------------------------------------------------------------------------------------
		private void dispose() {
//Reset the physics
			_rigidbody.velocity = Vector2.zero;
			_rigidbody.angularVelocity = 0;
			_rigidbody.rotation = 0;
			_rigidbody.simulated = false;

			transform.position = Vector2.zero;
			transform.rotation = Quaternion.identity;

			_renderer.enabled = false;
			
//If this bullet has smoke pfx we can't turn it off until they've finished up, nothing is easy :)
			if (_runningSmokePFX) {
				bulletSmokePFX.Stop();
				
				bulletFlamesPFX.Stop();
				bulletFlamesPFX.Clear();
				
//Using DOTween to do this is soooooo dirty, look to create your own state machine for the bullets
//Honestly, this is filth, it's a performance hit plus it'll create garbage
				DOVirtual.DelayedCall(2, () => {
					gameObject.hideFlags = HideFlags.HideInHierarchy;
					gameObject.SetActive(false);

//And return it to the pool            
					Owner.ReturnToPool(ID);
				});
				return;
			}

//Hide our bullet            
			gameObject.hideFlags = HideFlags.HideInHierarchy;
			gameObject.SetActive(false);

//And return it to the pool            
			Owner.ReturnToPool(ID);
		}

	}
}