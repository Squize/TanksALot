using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace DefaultNamespace.Attract {
	public class TitleScreen : MonoBehaviour {
		[SerializeField]
		private RectTransform logo;

		private TextMeshProUGUI _logoText;
		
		[SerializeField]
		private GameObject playButton;

		[SerializeField]
		private CanvasGroup playButtonCanvasGroup;
		
		[SerializeField]
		private Image ghost;

		[SerializeField]
		private CameraFilterPack_TV_Vignetting vignette;
		
		private Canvas _canvas;
		private CanvasGroup _canvasGroup;

//---------------------------------------------------------------------------------------
		private void Awake() {
			_canvas = GetComponent<Canvas>();
			_canvasGroup = GetComponent<CanvasGroup>();

			
			if (LoveSettings.Instance.BetterTopAndTails) {
				_logoText = logo.GetComponent<TextMeshProUGUI>();
				ghost.enabled = false;
				vignette.enabled = true;
				playButtonCanvasGroup.alpha = 0;
				playButton.SetActive(false);
				tweenInElements();
			}
		}
		
//---------------------------------------------------------------------------------------
		public void OnPlayPressed() {
			if (LoveSettings.Instance.BetterTopAndTails) {
				dispose();
			}
			else {
				_canvas.enabled = false;
				GameController.Instance.StartGame();
			}
		}
		
//---------------------------------------------------------------------------------------
		private void tweenInElements() {
//Hide everything before tweening it
			_logoText.alpha = 0;
			logo.DOScale(new Vector3(5, 5, 5), 0);
			
			float duration = 0.3f;
			float delay = 1.5f;

			logo.DOScale(Vector3.one, duration).SetDelay(delay)
				.SetEase(Ease.OutExpo);
			
			_logoText.DOFade(1,duration+0.5f).SetDelay(delay).SetEase(Ease.OutExpo);

//Trigger the screen shake			
			DOVirtual.DelayedCall(duration + delay-0.1f, () => {
				GameController.Instance.TriggerScreenShake(true);
			});

//...then turn it off again			
			DOVirtual.DelayedCall(duration + delay+0.3f, () => {
				GameController.Instance.TriggerScreenShake(false);
			});

			playButton.SetActive(true);
			playButtonCanvasGroup.DOFade(1, 0.5f).SetDelay(duration + delay + 1);
		}
		
//---------------------------------------------------------------------------------------
		private void dispose() {
//Fade down the vignette
			float duration = 0.5f;
			DOVirtual.Float(vignette.Vignetting, 0, duration, fadeDownVignette);
			DOVirtual.Float(vignette.VignettingDirt, 0, duration, fadeDownVignetteDirt);
			
//Fade down everything else
			_canvasGroup.DOFade(0, duration).OnComplete(() => {
//Kill everything off and start the game
				vignette.enabled = false;
				_canvas.enabled = false;
				
				CoverAll.Instance.FadeUp(titleScreenFadeDown);
//				GameController.Instance.StartGame();
			});
		}

//---------------------------------------------------------------------------------------
		private void titleScreenFadeDown() {
			CoverAll.Instance.FadeDown(0);
			GameController.Instance.StartGame();
		}
		
//---------------------------------------------------------------------------------------
		private void fadeDownVignette(float value) {
			vignette.Vignetting = value;
		}
		
//---------------------------------------------------------------------------------------
		private void fadeDownVignetteDirt(float value) {
			vignette.VignettingDirt = value;
		}
		
	}
}