using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace DefaultNamespace.Attract {
	public class GameOverHandler : MonoBehaviour {
		[SerializeField]
		private TextMeshProUGUI finalScoreText;
		
		[SerializeField]
		private Image ghost;

		[SerializeField]
		private CameraFilterPack_TV_Vignetting vignette;
		
		private Canvas _canvas;
		private CanvasGroup _canvasGroup;

//---------------------------------------------------------------------------------------
		private void Awake() {
			_canvas = GetComponent<Canvas>();
			_canvasGroup = GetComponent<CanvasGroup>();
			_canvas.enabled = false;
		}

//---------------------------------------------------------------------------------------
		public void GameOver(int score) {
			finalScoreText.text = "You Scored: "+score;
			
			_canvas.enabled = true;

			if (LoveSettings.Instance.BetterTopAndTails) {
				ghost.enabled = false;

				vignette.enabled = true;
				vignette.Vignetting = 0;
				vignette.VignettingDirt = 0;
				
				_canvasGroup.alpha = 0;
//Lets fade everything in, slight delay as we want to see the players explosion
				float duration = 2f;

				_canvasGroup.DOFade(1, 5).SetDelay(2);
				DOVirtual.Float(vignette.Vignetting, 0.3f, duration, fadeVignette).SetDelay(2);
				DOVirtual.Float(vignette.VignettingDirt, 0.338f, duration, fadeVignetteDirt).SetDelay(2);
			}
		}
		
//---------------------------------------------------------------------------------------
		public void OnRestartPressed() {
			if (LoveSettings.Instance.UseCoverAll) {
				CoverAll.Instance.FadeUp(restartAfterFade);
			}
			else {
				_canvas.enabled = false;
				GameController.Instance.StartGame();
			}
		}
		
//---------------------------------------------------------------------------------------
		private void restartAfterFade() {
			_canvas.enabled = false;
			vignette.enabled = false;

			CoverAll.Instance.FadeDown();
			
			GameController.Instance.StartGame();
		}
		
//---------------------------------------------------------------------------------------
		private void fadeVignette(float value) {
			vignette.Vignetting = value;
		}
		
//---------------------------------------------------------------------------------------
		private void fadeVignetteDirt(float value) {
			vignette.VignettingDirt = value;
		}		
	}
}